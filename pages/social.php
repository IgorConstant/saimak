<div class="uk-container uk-padding">
    <div class="title uk-text-center">
        <h1><strong>Contribuição Social</strong></h1>
        <p>A Saimak destina um percentual do resultado dos seus negócios <br> à unidades de proteção aos animais.</p>
    </div>
    <div class="block-middle-images">
        <div class="uk-grid-large uk-child-width-expand@s uk-text-center" uk-grid>
            <div>
                <div><img src="assets/images/dog-feet-2.png" alt="dog-feet"></div>
            </div>
            <div>
                <div><img src="assets/images/dog.png" alt="dog"></div>
            </div>
            <div>
                <div><img src="assets/images/dog-feet-1.png" alt="dog-feet"></div>
            </div>
        </div>
    </div>
</div>