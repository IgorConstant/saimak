<div class="uk-background-cover uk-panel">
    <div class="uk-container">
        <div class="block-title-banner">
            <h1>soluções <br> para integrar <br> <span class="initial">Homem,</span> <br> <span class="middle">Máquina</span> <br> <span class="last">e Resultado.</span> </h1>
        </div>

        <div class="block-img uk-text-center uk-hidden@l">
            <img src="assets/images/elemento.png" alt="elemento">
        </div>
    </div>
</div>