<nav class="uk-container" id="navbar" uk-navbar>
    <div class="uk-navbar-left">
        <a class="uk-navbar-item uk-logo" href="#"><img src="assets/images/brand.png" alt=""></a>
    </div>
    <div class="uk-navbar-right uk-visible@l">
        <ul class="uk-navbar-nav">
            <li><a class="scroll" href="#">Home</a></li>
            <li><a class="scroll" href="#sobre">Sobre</a></li>
            <li><a class="scroll" href="#quem-somos">Quem Somos</a></li>
            <li><a class="scroll" href="#servicos">Serviços</a></li>
            <li><a class="scroll" href="#galeria">Dispositivos</a></li>
            <li><a class="scroll" href="#contato">Contato</a></li>
        </ul>
    </div>
    <!-- Canvas -->
    <a class="uk-navbar-toggle uk-hidden@l uk-position-right" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-nav-primary"><span class="off-canvas"></span></a>
</nav>

<!-- OffCanvas -->
<div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar uk-flex uk-flex-column">
        <div class="brand uk-text-center">
            <a class="uk-offcanvas-brand" href=""><img width="80%" src="assets/images/brand.png" alt="brand-saimak"></a>
        </div>
        <ul class="uk-nav uk-nav-primary uk-padding-small uk-nav-left uk-margin-auto-vertical">
            <li><a class="scroll" href="#">Home</a></li>
            <li><a class="scroll" href="#sobre">Sobre</a></li>
            <li><a class="scroll" href="#quem-somos">Quem Somos</a></li>
            <li><a class="scroll" href="#servicos">Serviços</a></li>
            <li><a class="scroll" href="#galeria">Dispositivos</a></li>
            <li><a class="scroll" href="#contato">Contato</a></li>
        </ul>
    </div>
</div>