<div class="uk-container uk-padding">
    <div class="logo-intro uk-text-center">
        <img src="assets/images/brand-middle.png" alt="brand-middle">
    </div>
    <div class="title-atuacao uk-text-center">
        <h2>ÁREA DE ATUAÇÃO</h2>
        <p>Nossas unidades comerciais estão localizadas em pontos estratégicos das principais <br> regiões, o que nos permite a melhor logística de atendimento.</p>
    </div>
    <div class="map-area">
        <img src="assets/images/mapa.png" alt="mapa">
    </div>
    <div class="uk-flex uk-flex-center@s uk-flex-right@l uk-visible@l">
        <div class="contato">
            <div class="retangulo1"></div>
            <p class="contato1"><b>Ciro de Carvalho Jr.</b></p>
            <small>Comercial</small>
            <p class="uk-margin-remove contact-info"><i class="fas fa-phone-volume"></i> <a href="tel:+5541999176005">41 99917.6005</a> &nbsp;
                <i class="far fa-envelope"></i> <a href="mailto:ciro@saimak.com.br">ciro@saimak.com.br</a></p>
        </div>
        <div class="contato">
            <div class="retangulo2"></div>
            <p class="contato2"><b>José Paulo Verjas</b></p>
            <small>Comercial</small>
            <p class="uk-margin-remove contact-info"><i class="fas fa-phone-volume"></i> <a href="tel:+5511997773700">11 99777.3700</a> &nbsp; <i class="far fa-envelope"></i> <a href="mailto:paulo@saimak.com.br">paulo@saimak.com.br</a></p>
        </div>
    </div>

    <div class="uk-mobile-visible uk-hidden@l">
        <br>
        <div class="contato">
            <div class="retangulo1"></div>
            <p class="contato1"><b>Ciro de Carvalho Jr.</b></p>
            <small>Comercial</small>
            <p class="uk-margin-remove contact-info"><i class="fas fa-phone-volume"></i> <a href="tel:+5541999176005">41 99917.6005</a></p>
            <p class="uk-margin-remove contact-info"><i class="far fa-envelope"></i> <a href="mailto:ciro@saimak.com.br">ciro@saimak.com.br</a></p>
        </div>
        <br>
        <div class="contato">
            <div class="retangulo2"></div>
            <p class="contato2"><b>José Paulo Verjas</b></p>
            <small>Comercial</small>
            <p class="uk-margin-remove contact-info"><i class="fas fa-phone-volume"></i> <a href="tel:+5511997773700">11 99777.3700</a></p>
            <p class="uk-margin-remove contact-info"><i class="far fa-envelope"></i> <a href="mailto:paulo@saimak.com.br">paulo@saimak.com.br</a></p>
        </div>
    </div>
</div>