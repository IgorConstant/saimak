<div class="uk-container uk-container-large">
    <br><br><br>
    <div class="title uk-text-center">
        <img src="assets/images/brand-middle.png" alt="brand-middle">
        <h2>NOSSOS SERVIÇOS</h2>
    </div>
    <br>
    <div class="uk-grid-large uk-child-width-expand@l uk-text-center" uk-grid>
        <div>
            <div class="uk-padding">
                <img src="assets/images/maquina-exemplo.png" alt="maquina">
            </div>
            <div class="uk-text-center" uk-grid>
                <div class="uk-width-1-3 uk-width-1-6@l">
                    <img src="assets/images/icon-eletrica.png" alt="icon-eletrica">
                    <p>Elétrica</p>
                </div>
                <div class="uk-width-1-3 uk-width-1-6@l">
                    <img src="assets/images/icon-mecanica.png" alt="icon-mecanica">
                    <p>Mecânica</p>
                </div>
                <div class="uk-width-1-3 uk-width-1-6@l">
                    <img src="assets/images/icon-phm.png" alt="icon-phm">
                    <p>IHM</p>
                </div>
                <div class="uk-width-1-3 uk-width-1-6@l">
                    <img src="assets/images/icon-eletronica.png" alt="icon-eletronica">
                    <p>Eletrônica</p>
                </div>
                <div class="uk-width-1-3 uk-width-1-6@l">
                    <img src="assets/images/icon-pneumatica.png" alt="icon-pneumatica">
                    <p>Pneumática</p>
                </div>
            </div>
        </div>
        <div>
            <div class="card-area">
                <div class="uk-grid-collapse uk-child-width-expand@s uk-margin" uk-grid>
                    <div>
                        <div class="bg-text-green uk-text-left">
                            <p class="title"><img src="assets/images/checklist.png" alt="checklist"> &nbsp; Manutenção</p>
                            <p class="text">Elétrica; Mecânica; IHM; <br> Eletrônica e Pneumática.</p>
                        </div>
                    </div>
                    <div>
                        <div class="bg-1">

                        </div>
                    </div>
                </div>
                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin" uk-grid>
                    <div>
                        <div class="bg-text-green uk-text-left">
                            <p class="title"><img src="assets/images/checklist.png" alt="checklist"> &nbsp; Retrofit</p>
                            <p class="text">Atualização e recuperação total equipamentos inoperantes e/ou ultrapassados. É aproveitado
                                basicamente a estrutura mecânica.</p>
                        </div>
                    </div>
                    <div>
                        <div class="bg-2"></div>
                    </div>
                </div>
                <div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin" uk-grid>
                    <div>
                        <div class="bg-text-green uk-text-left">
                            <p class="title"><img src="assets/images/checklist.png" alt="checklist"> &nbsp; Capacitação Profissional</p>
                            <p class="text">Capacitação para colaboradores que possam garantir a disponibilidade operacional do equipamento.</p>
                        </div>
                    </div>
                    <div>
                        <div class="bg-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br><br>

<div class="before-and-after">
    <div class="uk-container uk-padding uk-text-center">
        <h2>ANTES E DEPOIS</h2>
        <br> <br>
        <div class="owl-carousel owl-theme">
            <div class="item">
                <img src="assets/images/galeria/maquina1-antes.png" href="#modal-zoom" uk-toggle alt="maquina1-antes">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina1-depois.png" href="#modal-zoom" uk-toggle alt="maquina1-depois">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina2-antes.png" href="#modal-zoom" uk-toggle alt="maquina2-antes">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina2-depois.png" href="#modal-zoom" uk-toggle alt="maquina2-depois">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina3-antes.png" href="#modal-zoom" uk-toggle alt="maquina3-antes">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina3-depois.png" href="#modal-zoom" uk-toggle alt="maquina3-depois">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina4-antes.png" href="#modal-zoom" uk-toggle alt="maquina4-antes">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina4-depois.png" href="#modal-zoom" uk-toggle alt="maquina4-depois">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina5-antes.png" href="#modal-zoom" uk-toggle alt="maquina5-depois">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina5-depois.png" href="#modal-zoom" uk-toggle alt="maquina5-depois">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina6-antes.png" href="#modal-zoom" uk-toggle alt="maquina6-antes">
            </div>
            <div class="item">
                <img src="assets/images/galeria/maquina6-depois.png" href="#modal-zoom" uk-toggle alt="maquina6-depois">
            </div>
        </div>
    </div>
</div>



<div id="modal-zoom" class="uk-modal-container uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-text-center">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="center: true">

            <ul class="uk-slider-items uk-grid uk-grid-match" uk-height-viewport="offset-top: true; offset-bottom: 30">
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/antes-maquina1.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/depois-maquina1.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/antes-maquina2.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/depois-maquina2.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/antes-maquina3.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/depois-maquina3.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/antes-maquina4.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/depois-maquina4.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/antes-maquina5.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/depois-maquina5.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/antes-maquina6.jpg" alt="" uk-cover>
                    </div>
                </li>
                <li class="uk-width-5-4">
                    <div class="uk-cover-container">
                        <img src="assets/images/galeria/1200x1200/depois-maquina6.jpg" alt="" uk-cover>
                    </div>
                </li>
            </ul>

            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

        </div>
    </div>
</div>


<div class="bg-green uk-padding">
    <div class="uk-container">
        <div class="cta-text uk-text-center">
            <h2>Está complicado?</h2>
            <h2 class="sub-title">Descomplique com a Saimak</h2>
            <br>
            <p>Conheça <strong>alguns dispositivos</strong> desenvolvidos e confeccionado</p>
        </div>
    </div>
</div>