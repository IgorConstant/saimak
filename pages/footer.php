<div class="uk-container uk-padding">
    <div class="uk-child-width-expand@m uk-text-center" uk-grid>
        <div>
            <div class="uk-text-left">
                <img src="assets/images/brand.png" alt="brand">
            </div>
            <ul class="uk-text-left">
                <li><img class="icon-phone" src="assets/images/phone-icon.png" alt="phone"> <a href="tel:+5511997773700"> (11) 99777-3700</a></li>
                <li><img class="icon-mail" src="assets/images/email-icon.png" alt="email"> <a href="mailto:contato@saimak.com.br">contato@saimak.com.br</a></li>
                <li><img class="icon-map" src="assets/images/map-icon.png" alt="map"> <a href="https://goo.gl/maps/2Q63FckwyVdoEq6U7" target="_blank">Matriz Salto - SP </a></li>
                <li class="endereco">Av. dos migrantes, 816 – Sala 71</li>
                <li class="endereco">CEP. 13322-170</li>
            </ul>
        </div>
        <div>
            <div>
                <h3 class="uk-text-left">Fale Conosco</h3>
                <form class="formphp" action="api/send-contact.php" name="form" method="POST">
                    <div class="uk-margin">
                        <input class="uk-input" type="text" name="nome" placeholder="Nome*" required>
                    </div>
                    <div class="uk-margin">
                        <input class="uk-input" type="tel" name="telefone" id="tel" placeholder="Telefone*" required>
                    </div>
                    <div class="uk-width-1-2@s">
                        <label class="dont-display">Se você não é um robô, deixe em branco.</label>
                        <input type="text" class="dont-display" name="leaveblank">
                    </div>
                    <div class="uk-width-1-2@s">
                        <label class="dont-display">Se você não é um robô, não mude este campo.</label>
                        <input type="text" class="dont-display" name="dontchange" value="http://">
                    </div>
                    <div class="uk-margin">
                        <input class="uk-input" type="email" name="email" placeholder="E-mail*" required>
                    </div>
                    <div class="uk-margin">
                        <textarea class="uk-textarea" rows="5" name="mensagem" placeholder="Mensagem*"></textarea>
                    </div>
                    <div class="btn-send uk-text-left">
                        <button class="uk-button uk-button-default" type="submit">Quero Saber Mais</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>