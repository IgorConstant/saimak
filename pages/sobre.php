<div class="uk-container-expand">
    <div class="uk-child-width-expand@m bg-gray" uk-grid>
        <div>
            <div class="intro-text uk-padding">
                <div class="title-about">
                    <h1>Conceito de <br> <span class="initial">Negócio</span></h1>
                </div>
                <div class="text-area">
                    <p>A Saimak atua no setor de injeção plástica, atendendo gestores que se vêem reféns dos grandes fornecedores do segmento, pelo tratamento indiferente que recebem na maioria dos casos.</p>
                    <p>Como na maioria das vezes nem sempre é possível simplesmente repassar ao consumidor final um reajuste de preços, nossa equipe soma qualificação e experiência de campo para observar, analisar, diagnosticar e propor soluções que busquem a melhor performance na integração Homem x Máquina x Resultado; gerando com isso maior margem de contribuição em cada item fabricado.</p>
                    <p>Quando analisada cada etapa do processo e pensando em cada detalhe; o resultado de produzir mais, melhor e com maior eficiência se torna algo simples e na maioria das vezes economicamente viável.</p>
                </div>
                <div class="btn-area">
                    <a class="uk-button uk-button-default scroll" href="#servicos">Quero Saber Mais</a>
                </div>
            </div>
        </div>
        <div class="bg-img">
            <div class="text-banner-col uk-flex uk-flex-middle uk-padding">
                <h3>Com a competitividade industrial cada vez mais acirrada, o que mais se deseja é a criação de algum diferencial que não seja apenas “preço e prazo”. <strong>É isso que a Saimak oferece para você.</strong></h3>
            </div>
        </div>
    </div>
</div>