<div class="uk-container uk-padding uk-text-center">
    <p>Garras / Máscaras</p>
    <br>
    <div class="grid-area">
        <div class="uk-text-center" uk-grid>
            <div class="uk-width-auto@m">
                <div><img style="cursor:pointer" class="uk-margin-bottom" src="assets/images/garras-mascaras-1.png" href="#modal-container-1" uk-toggle alt="garras-mascaras"></div>
            </div>
            <div class="uk-width-auto@m">
                <div><img style="cursor:pointer" class="uk-margin-bottom" src="assets/images/garras-mascaras-2.png" href="#modal-container-2" uk-toggle alt="garras-mascaras"></div>
            </div>
        </div>
    </div>

    <div class="grid-area-2">
        <div class="uk-text-center" uk-grid>
            <div class="uk-width-auto@m">
                <div>
                    <p>Pinças</p>
                    <img style="cursor:pointer" src="assets/images/pincas.png" href="#modal-container-3" uk-toggle alt="pinças">
                </div>
            </div>
            <div class="uk-width-auto@m">
                <div>
                    <p>Porta Ventosas</p>
                    <img style="cursor:pointer" src="assets/images/porta-ventosa.png" href="#modal-container-4" uk-toggle alt="porta-ventosa">
                </div>
            </div>
        </div>
    </div>
    <br>
    <a class="uk-button uk-button-default" href="#">Ver Galeria</a>

    <div id="modal-container-1" class="uk-modal-container" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <img src="assets/images/Mascara.jpg" alt="">
        </div>
    </div>

    <div id="modal-container-2" class="uk-modal-container" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <img src="assets/images/Garra.jpg" alt="">
        </div>
    </div>

    <div id="modal-container-3" class="uk-modal-container" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <img src="assets/images/Pinça.jpg" alt="foto-3">
        </div>
    </div>

    <div id="modal-container-4" class="uk-modal-container" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <img src="assets/images/porta-ventosa.jpg" alt="foto-3">
        </div>
    </div>
</div>
<br>
<br>