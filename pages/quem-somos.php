<div class="uk-container uk-padding uk-text-center">
    <div class="brand-intro ">
        <img src="assets/images/brand-middle.png" alt="brand-middle">
    </div>
    <div class="title">
        <h2>Quem <span>Somos</span></h2>
    </div>
</div>

<div class="bg-green uk-padding">
    <div class="uk-container">
        <div class="uk-grid-small uk-child-width-expand@m uk-text-center" uk-grid>
            <div>
                <div>
                    <img src="assets/images/jose-paulo-verjas.png" alt="jose-paulo-verjas">
                    <p>José Paulo Verjas</p>
                    <small>Comercial</small>
                    <p class="descricao-1">Vasta experiência em processos de injeção, materiais e oportunidades de otimização.</p>
                </div>
            </div>
            <div>
                <div>
                    <img src="assets/images/giancarlo-verjas.png" alt="giancarlo-verjas">
                    <p>Giancarlo Verjas</p>
                    <small>Operacional</small>
                    <p class="descricao-2">Experiência somada de 15 anos em manutenção de robôs multimarcas, nacionais e importados.</p>
                </div>
            </div>
            <div>
                <div>
                    <img src="assets/images/stefano-rossi.png" alt="stefano-rossi">
                    <p>Stefano Rossi</p>
                    <small>Administrativo</small>
                    <p class="descricao-3">Se preocupa com um atendimento atual e dinâmico, acompanhando assim a velocidade da Saimak.</p>
                </div>
            </div>
        </div>
    </div>
</div>