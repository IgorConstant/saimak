<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Agência Duetto - Itu/SP - https://www.duetto.ag/ ">
    <meta name="description" content="A Saimak atua no setor de injeção plástica, atendendo gestores que se vêem reféns dos grandes fornecedores do segmento, pelo tratamento indiferente que recebem na maioria dos casos.">

    <!-- Open Graph -->
    <meta property="og:locale" content="pt_BR">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Saimak">
    <meta property="og:site_name" content="Saimak">
    <meta property="og:description" content="A Saimak atua no setor de injeção plástica, atendendo gestores que se vêem reféns dos grandes fornecedores do segmento, pelo tratamento indiferente que recebem na maioria dos casos.">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;900&display=swap" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/main.css">

    <!-- FontAwesome -->
    <script src="https://kit.fontawesome.com/7bc0885a91.js"></script>

    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/images/brand-middle.png" type="image/x-icon">

    <title>Saimak</title>
</head>

<body>
    <header>
        <?php require('pages/header.php') ?>
    </header>

    <main>
        <section id="banner-intro">
            <?php require('pages/banner-intro.php') ?>
        </section>
        <section id="sobre">
            <?php require('pages/sobre.php') ?>
        </section>
        <section id="quem-somos">
            <?php require('pages/quem-somos.php') ?>
        </section>
        <section id="servicos">
            <?php require('pages/servicos.php') ?>
        </section>
        <section id="galeria">
            <?php require('pages/galeria.php') ?>
        </section>
        <section id="area-atuacao">
            <?php require('pages/area-atuacao.php') ?>
        </section>
        <section id="social">
            <?php require('pages/social.php') ?>
        </section>
    </main>

    <footer id="contato">
        <?php require('pages/footer.php') ?>
    </footer>

    <div class="copyright uk-text-center">
        <p class="uk-margin-remove">Copyright <?php echo date("Y"); ?> - Saimak - Todos os direitos reservados. Agência impactto</p>
    </div>

    <a class="scroll" id="myBtn" href="#banner-intro" title="Go to top"><i class="fas fa-chevron-up"></i></a>

    <!-- JS -->
    <script src="assets/js/main.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>

    <script type="text/javascript">
        $("#tel").mask("(00) 00000-0000");
    </script>
</body>

</html>